FROM golang:alpine
RUN apk update && \
    apk add apptainer
ENTRYPOINT ["/usr/bin/apptainer"]
